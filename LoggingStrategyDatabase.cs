﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Demos.DemosHelpers;

namespace Demos.Logging
{
    public class LoggingStrategyDatabase : LoggingStrategy
    {
        public override LoggingStrategies LoggingStrategyType { get { return LoggingStrategies.DATABASE; } }

        public string LoggingTableName
        {
            get
            {
                return this._loggingTableName;
            }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException();
                }
                else
                {
                    this._loggingTableName = value;
                }
            }
        }

        private string _loggingTableName = "Logging";

        public override string LoggedApplicationName { get; }

        private IDbConnection _connection = null;

        public LoggingStrategyDatabase(IDbConnection connection, string applicationName)
        {
            if (connection == null || String.IsNullOrEmpty(applicationName))
            {
                throw new ArgumentNullException();
            }

            this._connection = connection;
            this.LoggedApplicationName = applicationName;
        }

        public override void LogMessage(MessageTypes messageType, string message, string appVersion = "")
        {
            this.LogMessage(messageType.ToString(), message, "", appVersion);
        }

        public override void LogMessage(MessageTypes messageType, string message, string stackTrace, string appVersion = "")
        {
            this.LogMessage(messageType.ToString(), message, stackTrace, appVersion);
        }

        public override void LogCustomMessage(string customType, string message, string appVersion = "")
        {
            this.LogMessage(customType, message, "", appVersion);
        }

        private void LogMessage(string type, string message, string stackTrace, string appVersion)
        {
            string query = $@"
                INSERT INTO {this.LoggingTableName}(timestamp, messageType, message, stacktrace, appversion)
                VALUES ('{DateTime.Now.ToString(LoggingStrategy.UnifiedDateFormat)}', '{type}', '{message}', '{stackTrace}', '{appVersion}')
            ";

            UnifiedDatabaseHelperClass.ExecuteNonQuery(query, this._connection);
        }

    }
}
