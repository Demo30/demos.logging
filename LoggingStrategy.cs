﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Demos.Logging
{
    public abstract class LoggingStrategy
    {
        internal static string UnifiedDateFormat { get { return "yyyy-MM-dd HH:mm:ss"; } }

        public abstract LoggingStrategies LoggingStrategyType { get; }

        public abstract string LoggedApplicationName { get; }

        public abstract void LogMessage(MessageTypes messageType, string message, string appVersion);

        public abstract void LogMessage(MessageTypes messageType, string message, string stackTrace, string appVersion);

        public abstract void LogCustomMessage(string customType, string message, string appVersion);

        
    }
}
