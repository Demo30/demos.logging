﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;

namespace Demos.Logging
{
    public class LoggingStrategyFile : LoggingStrategy
    {
        public enum LogLimittingStrategies
        {
            NO_LIMIT,
            MESSAGE_COUNT,
            MESSAGE_DAYS
        }

        public override LoggingStrategies LoggingStrategyType { get { return LoggingStrategies.LOCAL_FILE; } }

        public override string LoggedApplicationName { get; }

        public LogLimittingStrategies Limit { get; set; } = LogLimittingStrategies.NO_LIMIT;
        public int Limit_Days
        {
            get { return (int)this._limit_days; }
            set
            {
                this._limit_days = value;
                this.Limit = LogLimittingStrategies.MESSAGE_DAYS;
            }
        }
        public int Limit_MessageCount
        {
            get { return (int)this._limi_messageCount; }
            set
            {
                this._limi_messageCount = value;
                this.Limit = LogLimittingStrategies.MESSAGE_COUNT;
            }
        }

        private int? _limit_days = null;
        private int? _limi_messageCount = null;

        private const string NODE_NAME_DOCUMENT_ROOT = "LoggingFile";
        private const string NODE_NAME_DOCUMENT_MESSAGES_ROOT = "LoggedMessages";
        private const string NODE_NAME_LOGGED_MESSAGE_ROOT = "LoggedMessage";
        private const string NODE_NAME_LOGGED_MESSAGE_TYPE = "MessageType";
        private const string NODE_NAME_LOGGED_MESSAGE_DATE = "MessageDate";
        private const string NODE_NAME_LOGGED_MESSAGE_CONTENT = "MessageContent";
        private const string NODE_NAME_LOGGED_MESSAGE_STACKTRACE = "MessageStackTrace";
        private const string NODE_NAME_LOGGED_MESSAGE_APPVERSION = "MessageAppVersion";


        private string _logging_filePath;

        public LoggingStrategyFile(string filePath, string applicationName)
        {
            if (String.IsNullOrEmpty(filePath) || String.IsNullOrEmpty(applicationName))
            {
                throw new ArgumentNullException();
            }

            this._logging_filePath = filePath;
            this.LoggedApplicationName = applicationName;
        }

        public override void LogMessage(MessageTypes messageType, string message, string appVersion)
        {
            this.LogMessageCore(messageType.ToString(), message, "", appVersion);
        }

        public override void LogMessage(MessageTypes messageType, string message, string stackTrace, string appVersion)
        {
            this.LogMessageCore(messageType.ToString(), message, stackTrace, appVersion);
        }

        public override void LogCustomMessage(string customType, string message, string appVersion)
        {
            this.LogMessageCore(customType, message, "", appVersion);
        }

        private void LogMessageCore(string type, string message, string stackTrace, string appVersion)
        {
            XmlDocument loggingFile = this.GetDocumentForWork(); ;
            XmlNode messagesRoot = loggingFile.GetElementsByTagName(NODE_NAME_DOCUMENT_MESSAGES_ROOT)[0];
            XmlElement newMessage = this.CreateMessageStructure(loggingFile, type, message, stackTrace, appVersion);
            messagesRoot.PrependChild(newMessage);

            loggingFile = this.LimitDocument(loggingFile);

            if (File.Exists(this._logging_filePath))
            {
                File.Delete(this._logging_filePath);
            }

            using (FileStream fs = new FileStream(this._logging_filePath, FileMode.CreateNew))
            {
                loggingFile.Save(fs);
            }
        }

        private XmlDocument LimitDocument(XmlDocument doc)
        {
            switch(this.Limit)
            {
                case LogLimittingStrategies.NO_LIMIT: return doc;
                case LogLimittingStrategies.MESSAGE_COUNT: return this.LimitByMessageCount(doc);
                case LogLimittingStrategies.MESSAGE_DAYS: return this.LimitByDays(doc);
                default: throw new InvalidOperationException($"No implementation for limiting strategy: {this.Limit}");
            }
        }

        private XmlDocument LimitByDays(XmlDocument doc)
        {
            XmlNodeList messages = doc.GetElementsByTagName(NODE_NAME_LOGGED_MESSAGE_ROOT);
            XmlNode messageListNode = doc.GetElementsByTagName(NODE_NAME_DOCUMENT_MESSAGES_ROOT)[0];

            XmlNode[] messagesArray = this.XmlNodeListToArray(messages);

            for (int i = 0; i < messagesArray.Length; i++)
            {
                XmlNode curNode = messagesArray[i];

                XmlNode dateNode = this.GetChildElementByTagName(NODE_NAME_LOGGED_MESSAGE_DATE, curNode);
                DateTime date = DateTime.Parse(dateNode.InnerText);
                int daysFromNow = (DateTime.Now - date).Days;

                if (daysFromNow >= this.Limit_Days)
                {
                    messageListNode.RemoveChild(curNode);
                }
            }

            return doc;
        }

        private XmlDocument LimitByMessageCount(XmlDocument doc)
        {
            XmlNodeList messages = doc.GetElementsByTagName(NODE_NAME_LOGGED_MESSAGE_ROOT);
            XmlNode messageListNode = doc.GetElementsByTagName(NODE_NAME_DOCUMENT_MESSAGES_ROOT)[0];

            XmlNode[] messagesArray = this.XmlNodeListToArray(messages);

            for (int i = 0; i < messagesArray.Length; i++)
            {
                XmlNode curNode = messagesArray[i];

                if (i >= this.Limit_MessageCount)
                {
                    messageListNode.RemoveChild(curNode);
                }
            }

            return doc;
        }

        private XmlDocument GetDocumentForWork()
        {
            XmlDocument loggingFile = null;

            if (!File.Exists(this._logging_filePath))
            {
                loggingFile = this.CreateNewLoggedDocument();
            }
            else
            {
                loggingFile = this.GetLoggedContent();
            }

            if (loggingFile == null)
            {
                throw new Exception("Failed to retreive or establish new logging file.");
            }

            return loggingFile;
        }

        private XmlDocument GetLoggedContent()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(this._logging_filePath);

            return doc;
        }

        private XmlDocument CreateNewLoggedDocument()
        {
            XmlDocument doc = new XmlDocument();

            XmlElement root = doc.CreateElement(NODE_NAME_DOCUMENT_ROOT);

            XmlElement loggingFileMeta = doc.CreateElement("LoggingFileMeta");
            XmlElement meta_creationDate = doc.CreateElement("DateOfCreation");
            XmlElement meta_applicationName = doc.CreateElement("LoggedApplicationName");

            meta_creationDate.InnerText = DateTime.Now.ToString(LoggingStrategy.UnifiedDateFormat);
            meta_applicationName.InnerText = this.LoggedApplicationName;

            loggingFileMeta.AppendChild(meta_creationDate);
            loggingFileMeta.AppendChild(meta_applicationName);

            XmlElement loggedMessagesRoot = doc.CreateElement(NODE_NAME_DOCUMENT_MESSAGES_ROOT);

            root.AppendChild(loggingFileMeta);
            root.AppendChild(loggedMessagesRoot);

            doc.AppendChild(root);
            return doc;
        }

        private XmlElement CreateMessageStructure(XmlDocument doc, string type, string message, string stackTrace, string appVersion)
        {
            XmlElement root = doc.CreateElement(NODE_NAME_LOGGED_MESSAGE_ROOT);

            XmlElement typeNode = doc.CreateElement(NODE_NAME_LOGGED_MESSAGE_TYPE);
            XmlElement dateNode = doc.CreateElement(NODE_NAME_LOGGED_MESSAGE_DATE);
            XmlElement messageContentNode = doc.CreateElement(NODE_NAME_LOGGED_MESSAGE_CONTENT);
            XmlElement messageStackTraceNode = doc.CreateElement(NODE_NAME_LOGGED_MESSAGE_STACKTRACE);
            XmlElement appVersionNode = doc.CreateElement(NODE_NAME_LOGGED_MESSAGE_APPVERSION);

            typeNode.InnerText = type;
            dateNode.InnerText = DateTime.Now.ToString(LoggingStrategy.UnifiedDateFormat);
            messageContentNode.InnerText = message;
            messageStackTraceNode.InnerText = stackTrace;
            appVersionNode.InnerText = appVersion;

            root.AppendChild(typeNode);
            root.AppendChild(dateNode);
            root.AppendChild(messageContentNode);
            root.AppendChild(messageStackTraceNode);
            root.AppendChild(appVersionNode);

            return root;
        }

        private XmlNode[] XmlNodeListToArray(XmlNodeList list)
        {
            XmlNode[] nodeArray = new XmlNode[list.Count];

            for (int i = 0; i < list.Count; i++)
            {
                nodeArray[i] = list[i];
            }

            return nodeArray;
        }

        private XmlNode GetChildElementByTagName(string tagName, XmlNode parent)
        {
            foreach(XmlNode child in parent.ChildNodes)
            {
                if (child.Name == tagName)
                {
                    return child;
                }
            }

            return null;
        }
    }
}
