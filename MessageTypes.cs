﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.Logging
{
    public enum MessageTypes
    {
        ERROR_INTERNAL,
        ERROR_EXTERNAL,
        WARNING,
        NOTICE
    }
}
