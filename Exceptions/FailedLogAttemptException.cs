﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.Logging
{
    public class FailedLogAttemptException : Exception
    {
        public FailedLogAttemptException()
        {

        }

        public FailedLogAttemptException(string message) : base(message)
        {

        }
    }
}
