﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.Logging
{
    public class LoggingCascade
    {
        public LoggingStrategy[] LoggingStrategies { get; }

        /// <summary>
        /// Attempts to log message using the provided logging strategies. If one failes, next in array follows. If all fail, custom exception is returned.
        /// </summary>
        /// <param name="loggingCascade"></param>
        public LoggingCascade(LoggingStrategy[] loggingCascade)
        {
            if (loggingCascade == null || loggingCascade.Length == 0)
            {
                throw new ArgumentNullException();
            }

            this.LoggingStrategies = loggingCascade;
        }

        public void LogMessage(MessageTypes type, string message, string stackTrace = "", string appVersion = "")
        {
            bool success = false;

            for (int i = 0; i < this.LoggingStrategies.Length; i++)
            {
                try
                {
                    LoggingStrategy str = this.LoggingStrategies[i];
                    str.LogMessage(type, message, stackTrace, appVersion);

                    success = true;
                    break;
                }
                catch
                {
                    success = false;
                    continue;
                }
            }

            if (!success)
            {
                throw new FailedLogAttemptException("Failed logging cascade.");
            }
        }

        public void LogCustomMessage(string customType, string message, string appVersion = "")
        {
            bool success = false;

            for (int i = 0; i < this.LoggingStrategies.Length; i++)
            {
                try
                {
                    LoggingStrategy str = this.LoggingStrategies[i];
                    str.LogCustomMessage(customType, message, appVersion);

                    success = true;
                    break;
                }
                catch
                {
                    success = false;
                    continue;
                }
            }

            if (!success)
            {
                throw new FailedLogAttemptException("Failed logging cascade.");
            }
        }
    }
}
